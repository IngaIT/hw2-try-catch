import {books} from "./books.js";

const root = document.getElementById('root');
const ul = document.createElement('ul');
root.append(ul);

books.forEach(book => {
    try {
        const neededProperties = ['author', 'name', 'price'];
        neededProperties.forEach(prop => {
            if (!Object.prototype.hasOwnProperty.call(book, prop)) {
                throw new Error(`Книга "${book.name}" має відсутні "${prop}" властивості`);
            }
        });
        const {author, name, price} = book;
        const li = document.createElement('li');
        li.textContent = `Автор: ${author}, Найменування: "${name}", Ціна: ${price}$`;
        ul.append(li);
    } catch (e) {
        console.log('Error: ', e.message);
    }
});